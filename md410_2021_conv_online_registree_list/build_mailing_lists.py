from collections import defaultdict

from md410_2021_conv_common_online import db

email_dict = defaultdict(list)

dbh = db.DB()
for reg in dbh.get_registrees():
    if reg.email:
        email_dict[reg.district].append(reg.email)

with open(f"md_410_convention_attendee_emails.txt", "w") as fmd:
    for (district, emails) in email_dict.items():
        emails = set(emails)
        with open(f"district_{district.lower()}_convention_attendee_emails.txt", "w") as fd:
            fmd.write(";".join(emails))
            fmd.write(";")
            fd.write(";".join(emails))


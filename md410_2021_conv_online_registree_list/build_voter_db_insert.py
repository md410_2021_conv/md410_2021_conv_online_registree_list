from collections import defaultdict

from md410_2021_conv_common_online import db

d = defaultdict(list)

dbh = db.DB()
with open(f"md_410_convention_voters_insert.sql", "w") as fmd:
    fmd.write("INSERT INTO voting_voter (identifier_text) VALUES ")
    fmd.write(
        ", ".join([f"({reg.reg_num})" for reg in dbh.get_registrees() if reg.voter])
    )
    fmd.write(";")

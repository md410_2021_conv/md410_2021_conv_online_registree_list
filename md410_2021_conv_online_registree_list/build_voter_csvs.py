from collections import defaultdict

from md410_2021_conv_common_online import db

d = defaultdict(list)

dbh = db.DB()
for reg in dbh.get_registrees():
    if reg.voter and reg.cell:
        d[reg.district].append([f"{reg.club} - {reg.name}", reg.cell])

with open(f"md_410_convention_voters.csv", "w") as fmd:
    fmd.write(f"Name,Phone\n")
    for (district, item) in d.items():
        with open(f"district_{district.lower()}_convention_voters.csv", "w") as fd:
            fd.write(f"Name,Phone\n")
            for (name, phone) in item:
                fmd.write(f"{district}: {name},{phone}\n")
                fd.write(f"{district}: {name},{phone}\n")
        

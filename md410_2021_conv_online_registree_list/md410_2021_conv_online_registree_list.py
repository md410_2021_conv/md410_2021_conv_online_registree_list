from collections import defaultdict
from datetime import datetime
import string

from openpyxl import Workbook
from openpyxl.styles.borders import Border, Side
from openpyxl.styles import Alignment
from openpyxl.styles import Font
from md410_2021_conv_common_online import db

import attr

THIN_BORDER = Border(
    left=Side(style="thin"),
    right=Side(style="thin"),
    top=Side(style="thin"),
    bottom=Side(style="thin"),
)

TITLE_HEIGHT = 40
NORMAL_HEIGHT = 25
BOLD = Font(name="Arial", bold=True)
CENTRE = Alignment(horizontal="center", vertical="center")
TEXT_COLUMNS = ["Name", "District", "Club"]
TEXT_WIDTH = 35
DEFAULT_WIDTH = 20
NOW = datetime.now()


@attr.s
class AttendanceSheet:
    sheet = attr.ib()
    convention = attr.ib()
    md = attr.ib(default=True)
    voter_num = attr.ib(default=0)
    attendee_num = attr.ib(default=0)

    def setup(self):
        self.sheet.page_setup.paperSize = self.sheet.PAPERSIZE_A4
        self.sheet.page_margins.top = 0.8
        self.sheet.page_margins.bottom = 0.2
        self.sheet.page_margins.left = 0.2
        self.sheet.page_margins.right = 0.2

        self.sheet.title = f"{self.convention} Attendance"

        row = 1
        self.sheet.append([f"{self.convention} Registrees as of {NOW:%d/%m/%Y %H:%M}"])
        self.sheet["A1"].font = BOLD
        self.sheet.row_dimensions[1].height = TITLE_HEIGHT
        headers = ["Last Name", "First Name", "Club", "First Convention", "Voter"]
        if self.md:
            headers.append("District")
        self.sheet.append(headers)

        row = 2
        for (n, column) in enumerate(headers):
            letter = string.ascii_uppercase[n]
            cell = f"{letter}{row}"
            self.sheet.column_dimensions[letter].width = (
                TEXT_WIDTH
                if any([c in column for c in TEXT_COLUMNS])
                else DEFAULT_WIDTH
            )
            self.sheet[cell].font = BOLD
            self.sheet[cell].alignment = CENTRE
            self.sheet[cell].border = THIN_BORDER
        self.sheet.row_dimensions[2].height = TITLE_HEIGHT

    def write_row(self, row):
        row_num = self.sheet.max_row + 1
        row = [
            reg.last_name,
            reg.first_names,
            reg.club,
            "Yes" if reg.first_mdc else "No",
            "Yes" if reg.voter else "No",
        ]
        if self.md:
            row.append(reg.district)
        for (col_num, col) in enumerate(row, 0):
            letter = string.ascii_uppercase[col_num]
            cell = f"{letter}{row_num}"
            self.sheet[cell].border = THIN_BORDER
            self.sheet[cell] = col
        self.sheet.row_dimensions[row_num].height = NORMAL_HEIGHT
        self.attendee_num += 1
        if reg.voter:
            self.voter_num += 1

    def finalise(self):
        row_num = self.sheet.max_row + 1
        self.sheet.append([f"Number of attendees: {self.attendee_num}"])
        self.sheet[f"A{row_num}"].font = BOLD
        row_num += 1
        self.sheet.append([f"Number of voters: {self.voter_num}"])
        self.sheet[f"A{row_num}"].font = BOLD


@attr.s
class VoterSheet:
    sheet = attr.ib()
    convention = attr.ib()
    voter_dict = attr.ib()

    def __attrs_post_init__(self):
        self.sheet.page_setup.paperSize = self.sheet.PAPERSIZE_A4
        self.sheet.page_margins.top = 0.8
        self.sheet.page_margins.bottom = 0.2
        self.sheet.page_margins.left = 0.2
        self.sheet.page_margins.right = 0.2

        self.sheet.title = f"{self.convention} Voting"

        self.sheet.append(
            [f"{self.convention} Voting details as of {NOW:%d/%m/%Y %H:%M}"]
        )
        self.sheet["A1"].font = BOLD
        self.sheet.row_dimensions[1].height = TITLE_HEIGHT
        headers = ["Club", "Number of Voters"]
        self.sheet.append(headers)

        row_num = 2
        for (n, column) in enumerate(headers):
            letter = string.ascii_uppercase[n]
            cell = f"{letter}{row_num}"
            self.sheet.column_dimensions[letter].width = TEXT_WIDTH
            self.sheet[cell].font = BOLD
            self.sheet[cell].alignment = CENTRE
            self.sheet[cell].border = THIN_BORDER
        self.sheet.row_dimensions[2].height = TITLE_HEIGHT

        clubs = list(self.voter_dict.keys())
        clubs.sort()
        for club in clubs:
            row_num += 1
            for (col_num, col) in enumerate([club, self.voter_dict[club]], 0):
                letter = string.ascii_uppercase[col_num]
                cell = f"{letter}{row_num}"
                self.sheet[cell].border = THIN_BORDER
                self.sheet[cell] = col
            self.sheet.row_dimensions[row_num].height = NORMAL_HEIGHT

        row_num += 1
        self.sheet.append([f"Number of clubs: {len(self.voter_dict)}"])
        self.sheet[f"A{row_num}"].font = BOLD
        row_num += 1
        self.sheet.append([f"Number of voters: {sum(self.voter_dict.values())}"])
        self.sheet[f"A{row_num}"].font = BOLD


dbh = db.DB()

wb = Workbook()
dest_filename = "registrees_list.xlsx"

md_sheet = AttendanceSheet(wb.active, "MD410", True)
md_sheet.setup()
attendance_sheets = {"md": md_sheet}
voters = defaultdict(lambda: defaultdict(int))
rows = [(reg.last_name, reg) for reg in dbh.get_registrees()]
rows.sort()
for (_, reg) in rows:
    if reg.district not in attendance_sheets:
        sheet = AttendanceSheet(wb.create_sheet(), reg.district, False)
        attendance_sheets[reg.district] = sheet
        sheet.setup()
    if reg.attending_md_convention:
        attendance_sheets["md"].write_row(reg)
    if reg.attending_district_convention:
        attendance_sheets[reg.district].write_row(reg)

    if reg.voter:
        voters[reg.district][reg.club] += 1

for a in attendance_sheets.values():
    a.finalise()

names = [n for n in attendance_sheets.keys() if "md" not in n]
names.sort()
for (offset, name) in enumerate(names, 1):
    wb.move_sheet(attendance_sheets[name].sheet, offset)

districts = list(voters.keys())
districts.sort()
for d in districts:
    VoterSheet(wb.create_sheet(), d, voters[d])


wb.save(filename=dest_filename)
